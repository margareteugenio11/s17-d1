// Create array
// Use an array literal: []

const array1 = ['eat', 'sleep'];

// Use the new keyword
const array2 = new Array('pray', 'play');

// empty array
const myList = [];

// array of numbers
const numArray = [2,3,4,5,6];

// array of mstrings
const stringArray = ['eat', 'work', 'play', 'pray'];

// array of mixed
const newData = ['work', 1, true];

const newData1 = [
	{'task': 'exercise'},
	[1,2,3],
	function hello(){
		console.log('hi I am array.')
	}
]
console.log(newData1);

/* Mini-Activity

	Create an array with 7 items all strings.
		-list seven of the places you want to visit someday.
		-Log the first item in the console.
		-Log all the items in the console.
*/

let arrayPlace = ['Afghanistan', 'Zimbabwe', 'North Korea', 'Japan', 'Amsterdam', 'Pakistan', 'Korea'];

console.log(arrayPlace[0]);
console.log(arrayPlace);

for(let i = 0; i < arrayPlace.length; i++){
	console.log(arrayPlace[i]);
}

//Array manipulation
// - push() 
//Adds element to an array at the end of the array

let dailyActivites = ['eat', 'work', 'pray', 'play'];
dailyActivites.push('excercise');
console.log(dailyActivites);

// - unshift 
// Adds element to an array at the beginning of the array

dailyActivites.unshift('sleep');
console.log(dailyActivites);

dailyActivites[2] = 'sing';
console.log(dailyActivites);

dailyActivites[6] = 'dance';
console.log(dailyActivites);

// Re-assign the values/items in an array:
arrayPlace[3] = 'Giza Sphinx';
console.log(arrayPlace);

/*Mini Activity
	Rea-assign the values for the first and last item in the array.
			-Re-assign it with your hometown and your highschool
			-Log the array in the console.
			-Log the first and last items in the console.
*/

arrayPlace[0] = 'Manila';
arrayPlace[arrayPlace.length-1] = 'St. Catherine';
console.log(arrayPlace[0]);
console.log(arrayPlace[arrayPlace.length-1]);

// Adding items in an array without using methods:
let array = [];
console.log(array[0]);
array[0] = 'Cloud Strife';
console.log(array[0]);
console.log(array [1]);
array [1] = 'Tifa Lockhart';
console.log(array[1]);
array[array.length-1] = 'Aerith Gainsborough';
console.log(array);
array[array.length] = 'Vincent Valentine';
console.log(array);

// Array Methods
	// Manipulate array with pre-determined JS functions.
	// Mutators - these arrays methods usually change the original array.

	let arrayX = ['Juan', 'Pedro', 'Jose', 'Andres'];
	// without method
	arrayX[arrayX.length] = 'Francisco';
	console.log(arrayX);

	//.push() - allows us to add an element at the end of the array.
	arrayX.push('Elmo');
	console.log(arrayX);

	// .unshift() - allows us to add an element at the beginning of the array.
	arrayX.unshift('Simon');
	console.log(arrayX);

	// .pop() - allows us to delete or remove the last item/element at the end of the array.
	arrayX.pop();
	console.log(arrayX);

	//.pop() is also able to return the item we removed.
	console.log(arrayX.pop());
	console.log(arrayX);

	let removedItem = arrayX.pop();
	console.log(arrayX);
	console.log(removedItem);

	// .shift() return item we removed
	let removedItemShift = arrayX.shift();
	console.log(arrayX);
	console.log(removedItemShift);

/*Mini-Activity

	Remove the first item of arrayX and log arrayX in the console.
	Delete the last item of arrayX and log arrayX in the console.
	add "george" at the start of the arrayX and log arrayX in the console.
	add "michael" at the end of the arrayX and log arrayX in the console.

	use array methods.
*/

	arrayX.pop();
	console.log(arrayX);
	arrayX.shift();
	console.log(arrayX);
	arrayX.push('Michael');
	console.log(arrayX);
	arrayX.unshift('George');
	console.log(arrayX);

	//.sort() - by default, will allow us to sort our items in ascending order.

	arrayX.sort();
	console.log(arrayX);

	let numArray1 = [3,2,1];
	numArray1.sort();
	console.log(numArray1);

	let numArray2 = [32, 400, 450, 100, 203, 4923, 240, 69];
	numArray2.sort((a,b)=>a-b);
	console.log(numArray2);
	// .sort() converts all items in to string items accordingly as if they are string.

	// ascending sort per number's value
	numArray2.sort(function(a,b){
		return a-b;
	})

	console.log(numArray2);

	let arrayStr = ['Marie', 'Zen', 'Jamie', 'Elaine'];
	arrayStr.sort();
	console.log(arrayStr);

	// .reverse() - reversed the order of the items.
	arrayStr.sort().reverse();
	console.log(arrayStr);

	//.splice() - allows us to remove and add elements from a given index.
	// Syntax: array.splice(startingIndex, numberofItemstobeDeleted, elementstoAdd)
	let lakersPlayers = ['Lebron', 'Davis', 'Westbrook', 'Kobe', 'Shaq'];

	lakersPlayers.splice(0, 0, 'Carusso');
	console.log(lakersPlayers);

	lakersPlayers.splice(0,1);
	console.log(lakersPlayers);

	lakersPlayers.splice(0, 3);
	console.log(lakersPlayers);


	lakersPlayers.splice(1,1);
	console.log(lakersPlayers);

	lakersPlayers.splice(1,0,'Gasol','Fisher');
	console.log(lakersPlayers);

	//Non-Mutators
		//Methods that will not change the original Array.

	// .slice() - allows us to get a portion of the original array and return a new array with the items selected from the original.
	// syntax: slice(startIndex, endIndex)

	let computerBrands = ['IBM', 'HP', 'Apple', 'MSI'];
	computerBrands.splice(2, 2, 'Compaq', 'Toshiba', 'Acer');
	console.log(computerBrands);

	let newBrands = computerBrands.slice(1,3);
	console.log(computerBrands);
	console.log(newBrands);

	let fonts = ['Times New Roman', 'Comic San MS', 'Impact', 'Monotype Corsiva', 'Arial', 'Arial Black'];

	let newFontSet = []
	newFontSet = fonts.slice(1,4);
	console.log(newFontSet);

	let newFontSet1 = fonts.slice(2);
	console.log(newFontSet1);

	/*
		Mini-Activity
		Given a set of data, use slice to copy the last two items in the array.
		Save the sliced portion of the array into a new variable:
			- microsoft (last two)
		use slice to copy the third and fourth item in the array.
		Save the sliced portion of the array into a new variable:
			- nintendo. (3rd 4th)
		log both new arrays in the console.
	*/


let videoGame = ['PS4', 'PS5', 'Switch', 'Xbox', 'Xbox1'];
	let microsoft = videoGame.slice(videoGame.length-2);
	let nintendo = videoGame.slice(2,4);
	console.log(microsoft);
	console.log(nintendo);

	// .toString() - convert the array into a single value.
	// syntax: array.toString();

	let sentence = ['I', 'like', 'Javascript', '.', 'It', 'is', 'fun', '.'];
	let sentenceString = sentence.toString();
	console.log(sentence);
	console.log(sentenceString);

	//.join() - converts the array into a single value as a string but separator can be specified.
	// syntax: array.join(separator)
	let sentence2 = ['My', 'favorite', 'fastfood', 'is', 'Botejyu'];
	let sentenceString2 = sentence2.join(' ');
	console.log(sentenceString2);
